# org.plomgrading.PlomClient: Flatpak support

This a Flatpak manifest for the [Plom](https://plomgrading.org) Client.


## End-user experience

Installation: once its on flathub.org, it'll appear in apps like
"Gnome Software" or be easily installed from flathub.org.

Use: `flatpak run org.plomgrading.PlomClient`.

Or `flatpak run --command=plom-manager org.plomgrading.PlomClient`.


## Dev experience

I installed the `org.kde.Platform` and `org.kde.Sdk`.  I think this is
recommended for Qt apps.

Then basically just keep running these two commands and hacking the
json until it worked:
```
flatpak-builder --repo=repo --force-clean build-dir org.plomgrading.PlomClient.json
flatpak-builder --run build-dir org.plomgrading.PlomClient.json
```
This is following the tutorial [firstflatpak].

Finally, to install:
```
flatpak --user remote-add --no-gpg-verify tutorial-repo repo
flatpak --user install tutorial-repo org.plomgrading.PlomClient
```


## TODO

- [x] proof of concept
- [x] need appdata.xml, ideally in-tree.
- [x] need icon
- [ ] submit to flathub


## Credits

Module for building PyQt5 is a copy-pasta from [vidcutter].

For the python dependencies, I used [flatpak-pip-generator].  Doesn't seem
to have an installer: I just copied the single Python file off github.


[firstflatpak]: https://docs.flatpak.org/en/latest/first-build.html
[vidcutter]: https://github.com/flathub/com.ozmartians.VidCutter/blob/master/com.ozmartians.VidCutter.json
[flatpak-pip-generator]: https://github.com/flatpak/flatpak-builder-tools/blob/master/pip/flatpak-pip-generator
